import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

PhoneData = new Mongo.Collection('phonedata');


Router.route('/', {
	name: 'home',
	template: 'home'	
});

// Router.configure({
// 	layoutTemplate: 'main'
// });

Meteor.methods({
	'generateAccelerometerGraph'(){
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		

		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].aa_x};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].aa_y};
			yArray.push(yObj);
		}


		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].aa_z};
			zArray.push(zObj);
		}

		// var firstTime = phoneArray[0].sensor_infos[0].t;
		// var lastTime = phoneArray[0].sensor_infos[count-1].t;	
		// var timeRange = lastTime - firstTime;
		// console.log("Time Range is: ",timeRange);
		
		
		var chart = new CanvasJS.Chart("chartContainer",
		{
		 animationEnabled: true,
		 title:{
		   text: "Accelerometer Data"
		 },
		 data: [
		 {
		   name:"X-axis",
		   labelFontSize: 1,
		   labelMaxWidth: 4,
		   type: "line", //change type to bar, line, area, pie, etc
		   showInLegend: true,
		   dataPoints: xArray
		   },
		   {
		   name:"Y-axis",
		   type: "line",
		   showInLegend: true,        
		   dataPoints: yArray
		   },
		   {
		   name:"Z-axis",
		   type: "line",
		   showInLegend: true,        
		   dataPoints: zArray
		   }
		 ],
		 legend: {
		   cursor: "pointer",
		   itemclick: function (e) {
		     if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		       e.dataSeries.visible = false;
		     } else {
		       e.dataSeries.visible = true;
		   }
		   chart.render();
		   }
		 }
	});
	chart.render();
	},
	'generateMagnetometerGraph'(){
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].mm_x};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].mm_y};
			yArray.push(yObj);
		}


		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].mm_z};
			zArray.push(zObj);
		}
		var chart = new CanvasJS.Chart("chartContainer",
		{
		 animationEnabled: true,
		 title:{
		   text: "Magnetometer Data"
		 },
		 data: [
		 {
		   name:"X-axis",
		   labelFontSize: 1,
		   labelMaxWidth: 4,
		   type: "line", //change type to bar, line, area, pie, etc
		   showInLegend: true,
		   dataPoints: xArray
		   },
		   {
		   name:"Y-axis",
		   type: "line",
		   showInLegend: true,        
		   dataPoints: yArray
		   },
		   {
		   name:"Z-axis",
		   type: "line",
		   showInLegend: true,        
		   dataPoints: zArray
		   }
		 ],
		 legend: {
		   cursor: "pointer",
		   itemclick: function (e) {
		     if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		       e.dataSeries.visible = false;
		     } else {
		       e.dataSeries.visible = true;
		   }
		   chart.render();
		   }
		 }
	});
	chart.render();
	},
	'generateMotionRotationGraph'(){
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].mrr_x};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].mrr_y};
			yArray.push(yObj);
		}


		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].mrr_z};
			zArray.push(zObj);
		}
		var chart = new CanvasJS.Chart("chartContainer",
		{
		 animationEnabled: true,
		 title:{
		   text: "Motion Rotation Rate Data"
		 },
		 data: [
		 {
		   name:"X-axis",
		   labelFontSize: 1,
		   labelMaxWidth: 4,
		   type: "line", //change type to bar, line, area, pie, etc
		   showInLegend: true,
		   dataPoints: xArray
		   },
		   {
		   name:"Y-axis",
		   type: "line",
		   showInLegend: true,        
		   dataPoints: yArray
		   },
		   {
		   name:"Z-axis",
		   type: "line",
		   showInLegend: true,        
		   dataPoints: zArray
		   }
		 ],
		 legend: {
		   cursor: "pointer",
		   itemclick: function (e) {
		     if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		       e.dataSeries.visible = false;
		     } else {
		       e.dataSeries.visible = true;
		   }
		   chart.render();
		   }
		 }
	});
	chart.render();
	},
	'generateMotionUserAccelerationGraph'(){
		var phoneArray = PhoneData.find().fetch();
		var count = phoneArray[0].sensor_infos.length;
		var xArray = [];
		for(var i=0; i<count; i++) {
			var xObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].mua_x};
			xArray.push(xObj);
		}

		var yArray = [];
		for(var i=0; i<count; i++) {
			var yObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].mua_y};
			yArray.push(yObj);
		}


		var zArray = [];
		for(var i=0; i<count; i++) {
			var zObj = {x: phoneArray[0].sensor_infos[i].t, y: phoneArray[0].sensor_infos[i].mua_z};
			zArray.push(zObj);
		}
		var chart = new CanvasJS.Chart("chartContainer",
		{
		 animationEnabled: true,
		 title:{
		   text: "Motion User Acceleration Rate Data"
		 },
		 data: [
		 {
		   name:"X-axis",
		   labelFontSize: 1,
		   labelMaxWidth: 4,
		   type: "line", //change type to bar, line, area, pie, etc
		   showInLegend: true,
		   dataPoints: xArray
		   },
		   {
		   name:"Y-axis",
		   type: "line",
		   showInLegend: true,        
		   dataPoints: yArray
		   },
		   {
		   name:"Z-axis",
		   type: "line",
		   showInLegend: true,        
		   dataPoints: zArray
		   }
		 ],
		 legend: {
		   cursor: "pointer",
		   itemclick: function (e) {
		     if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		       e.dataSeries.visible = false;
		     } else {
		       e.dataSeries.visible = true;
		   }
		   chart.render();
		   }
		 }
	});
	chart.render();
	}


});

Router.route('/chartContainer');

if(Meteor.isServer) {
	Meteor.publish('phone_data', function(){
		return PhoneData.find({});
	});


	Router.map( function () {
  		this.route('codeEditor',{
    		waitOn: function(){
        		return [IRLibLoader.load('http://canvasjs.com/assets/script/canvasjs.min.js')]
    		}
  		});
	});
	Meteor.methods({
		loadCanvasJS: function() {
			this.unblock();
			return Meteor.http.call("GET", "http://canvasjs.com/assets/script/canvasjs.min.js");
		}
	});
}

if(Meteor.isClient) {
	Meteor.startup(function(){
		Session.set('data_loaded', false);
	});
	Meteor.subscribe('PhoneData', function(){
		Session.set('data_loaded', true);
	});
	



	Template.querySelect.events({
		'submit form':function(event){
			event.preventDefault();
			var experiment = event.target.experiments.value;
			var session = event.target.sessions.value;
			var parameter = event.target.parameters.value;
			Session.set("selectedParameter", parameter);
			if(parameter=="Accelerometer") {
				console.log("parameter matched 'Accelerometer'");
				Meteor.call('generateAccelerometerGraph');
			}
			else if(parameter=="Magnetometer") {
				Meteor.call('generateMagnetometerGraph');
			}
			else if(parameter=="Motion Rotation Rate") {
				Meteor.call('generateMotionRotationGraph');
			}
			else if(parameter=="Motion User Acceleration") {
				Meteor.call('generateMotionRotationGraph');
			}
		}
	});


	Template.dataItem.helpers({
		'getAccelerometerValues': function()  {	
			var phoneArray = PhoneData.find().fetch();
			var count = phoneArray[0].sensor_infos.length;
			console.log("count is ",count);
			var accelerometerArray = [count];
			var i = 0;
			for(i=0; i<count; i++) {
				console.log("accelerometerArray for loop entered");
				accelerometerArray[i] = phoneArray[0].sensor_infos[i].aa_x;
				console.log(accelerometerArray[i]);
			}
			return accelerometerArray;
		}
	});
}